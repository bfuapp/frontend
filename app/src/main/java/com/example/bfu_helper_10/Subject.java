package com.example.bfu_helper_10;

import android.util.Log;

public class Subject {
    private String time, subject, lecturer, room;
    private int image;

    public Subject(String time, String subject, String lecturer, String room, int image) {
        this.time = time;
        this.subject = subject;
        this.image = image;
        this.lecturer = lecturer;
        this.room = room;
    }

    public String getTime() {
        return time;
    }

    public String getSubject() {
        return subject;
    }

    public String getLecturer() {
        return lecturer;
    }

    public String getRoom() {
        return room;
    }

    public int getImage() {
        return image;
    }

    static public int getPictureBasedOnName(String name)
    {
        //Log.d("string testing", name);
        if (name.equals("Функциональный анализ (пр.)") || name.equals("Функциональный анализ (лек.)"))
            return R.raw.fuan;
        else
        if (name.equals("Базы данных (лек.)"))
            return R.raw.database;
        else
        if (name.equals("Уравнения математической физики (лек.)") || name.equals("Уравнения математической физики (пр.)"))
            return R.raw.phys;
        else
        if (name.equals("Параллельное программирование в задачах визуализации данных (лек.)"))
            return R.raw.gpu;
        else
        if (name.equals("Элективные курсы по физической культуре"))
            return R.raw.fizra;
        else
        if (name.equals("Android"))
            return R.raw.android;
        else
            return R.raw.index;
    }
}
