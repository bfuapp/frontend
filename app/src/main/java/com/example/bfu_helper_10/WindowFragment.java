package com.example.bfu_helper_10;

import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GestureDetectorCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class WindowFragment extends BottomSheetDialogFragment  {

    private RatingBar rBar;
    private TextView tView;
    private Button btn;
    private TextView header;
    private TextView lecturer;
    private View _view;

    private String _header = "";
    private String _lecturer = "";
    private String __time = "";
    private String sch_day_of_week = "";
    private String sch_day = "";
    private String sch_month = "";
    private int __image = 0;

    private GestureDetectorCompat mGestureDetector = null;

    public static final String TAG = "ActionBottomDialog";

    public static WindowFragment newInstance() {
        //Log.d("test", "newInstance triggered");
        return new WindowFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState)
    {
        _view =  inflater.inflate(R.layout.fragment_window, container, false);

        //Log.d("test", "onCreate triggered");
        header = (TextView) _view.findViewById(R.id.bdHeader);
        lecturer = (TextView) _view.findViewById(R.id.bdTeacher);
        ImageButton image = _view.findViewById(R.id.bdButton);


        header.setText(_header);
        lecturer.setText(_lecturer);
        image.setImageResource(__image);

        _view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

//                if(event.getAction() == MotionEvent.ACTION_DOWN){
//                    ((MainActivity)getActivity()).closeBottomSheet();
//                }

                mGestureDetector.onTouchEvent(event);
                return true;
            }
        });

        mGestureDetector = new GestureDetectorCompat(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
            {
                Log.d("touch_event", "onFling");
                //((MainActivity)getActivity()).closeBottomSheet();
                return false;
            }
            @Override
            public boolean onDown(MotionEvent e)
            {
                Log.d("touch_event", "onDown");

                return false;
            }
        });

        return _view;
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)  {
        super.onViewCreated(view, savedInstanceState);

        TextView rating = (TextView) getView().findViewById(R.id.bdText2);
        TextView private_rating = (TextView) getView().findViewById(R.id.bdText5);
        rBar = (RatingBar) getView().findViewById(R.id.ratingBar1);

        TextView date_to_set = (TextView) getView().findViewById(R.id.bdDate);

        // capitalize first letter, and remaining part is in lower case
        date_to_set.setText(sch_day_of_week.substring(0, 1) + sch_day_of_week.substring(1).toLowerCase() + ", " + sch_day + " " + sch_month);

        rBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            // Called when the user swipes the RatingBar
            @Override
            public void onRatingChanged(RatingBar ratingBar, float _rating, boolean fromUser) {
                float score = ratingBar.getRating();
                rating.setText("Рейтинг занятия: " + score);
                private_rating.setText("Ваша оценка: " + score);
            }
        });
    }

    public void TestFoo(String __header, String __lecturer, String time, int image, String _dayOfWeek, String _day, String _month)
    {
        _header = __header;
        _lecturer = __lecturer;
        __time = time;
        __image = image;
        sch_month = _month;
        sch_day = _day;
        sch_day_of_week = _dayOfWeek;

        this.show(MainActivity.fragmentManager, "tag");
    }
}