package com.example.bfu_helper_10;

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.google.gson.JsonParser
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.coroutineContext


class HttpController
{
    companion object Functions
    {

        private fun parseJSON(jsonString: String, daysOfTheWeek: List<String>) : MutableList<Day>
        {

            val parsedData = mutableListOf<Day>()
            val parser = JsonParser()
            val days = parser.parse(jsonString).asJsonObject["daysOfWeek"].asJsonArray
            for ((index, value) in days.withIndex())
            {

                val day = Day(daysOfTheWeek[index])
                val classes = value.asJsonArray
                for (lecture in classes)
                {

                    day.addAClass(Class(lecture.asJsonObject["name"].asString,
                            lecture.asJsonObject["time"].asString,
                            lecture.asJsonObject["room"].asString,
                            lecture.asJsonObject["lecturer"].asString))

                }

                parsedData.add(day)

            }

            return parsedData

        }

        @JvmStatic
        fun getDaysOfTheWeek(link: String, requestBody: String, context: Context, textView: RecyclerView, days: ArrayList<DayOfSchedule>)// Put view as arguement
        {
            var start_date = "2020-09-01" // Start date
            var end_date = "2020-12-31"

            val daysOfTheWeek = listOf<String>(
                    context.getString(R.string.monday),
                    context.getString(R.string.tuesday),
                    context.getString(R.string.wednesday),
                    context.getString(R.string.thursday),
                    context.getString(R.string.friday),
                    context.getString(R.string.saturday),
                    context.getString(R.string.sunday)
            )

            val date_format = SimpleDateFormat("yyyy-MM-dd")
            val schedule_calendar = Calendar.getInstance()
            schedule_calendar.time = date_format.parse(start_date)

            val request = object  : StringRequest(Request.Method.POST, link,
                    Response.Listener<String> { response ->

                        val _days = parseJSON(response, daysOfTheWeek)

                        var go = true
                        while(go)
                            for ((y, day) in _days.withIndex()) {

                                // sunday is a first day, monday - second, ..., so we need to shift it to the right, so that monday becomes 1
                                if ((schedule_calendar.get(Calendar.DAY_OF_WEEK)+5) % 7 != y)
                                    continue

                                var dayNumber = schedule_calendar.get(Calendar.DAY_OF_MONTH)
                                var monthName = schedule_calendar.getDisplayName(Calendar.MONTH,  Calendar.LONG, Locale.getDefault())

                                if (date_format.format(schedule_calendar.time).equals(end_date))
                                {
                                    go = false
                                    break
                                }

                                val subjects: MutableList<Subject> = ArrayList()
                                for (clazz in day.classes) {

                                    subjects.add(Subject(clazz.time, clazz.name, clazz.lecturer, clazz.room, Subject.getPictureBasedOnName(clazz.name)))
                                }

                                days.add(DayOfSchedule(daysOfTheWeek[y], monthName, dayNumber, subjects))
                                schedule_calendar.add(Calendar.DATE, 1) // number of days to add
                        }

                    },
                    Response.ErrorListener { error ->
                        print("ERROR")
                    }
            ) {
                override fun getBodyContentType(): String {
                    return "application/json"
                }

                @Throws(AuthFailureError::class)
                override fun getBody(): ByteArray {
                    return requestBody.toByteArray()
                }

                override fun getParams(): MutableMap<String, String> {
                    super.getParams().put("Accept", "*/*")
                    return super.getParams()
                }

            }

            HttpRequestManager.getInstance(context).addToRequestQueue(request)

        }

//        fun getImage(link : String,imageView: ImageView, context: Context )
//        {
//
//            val imageByteCode = object  : StringRequest(Request.Method.GET, link,
//
//                Response.Listener<String> { response ->
//                    val aaa = BitmapFactory.decodeByteArray(response.toByteArray(), 0,   response.toByteArray().size)
//                    imageView.setImageBitmap(Bitmap.createScaledBitmap(aaa,200,200,false))
//
//                },
//                Response.ErrorListener { error ->
//                    print("ERROR")
//                }
//            ) {
//
//                override fun getParams(): MutableMap<String, String> {
//                    super.getParams().put("Accept","*/*")
//                    return super.getParams()
//                }
//
//            }
//
//            HttpRequestManager.getInstance(context).addToRequestQueue(imageByteCode)
//
//        }

    }

}