package com.example.bfu_helper_10;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.tv.TvContract;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RemoteViews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.os.Bundle;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

/**
 * Implementation of App Widget functionality.
 */
public class activity_login extends AppCompatActivity {

    private EditText textUsernameLayout;
    private EditText textPasswordInput;
    private Button loginButton;
    private ProgressBar progressBar;
    private ImageView LogoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // use specific view (activity_login.xml) for this activity
        setContentView(R.layout.activity_login);


        // find view info in res/layout/*
        textUsernameLayout = findViewById(R.id.textUsernameLayout);
        textPasswordInput = findViewById(R.id.textPasswordInput);
        loginButton = findViewById(R.id.loginButton);
        //progressBar = findViewById(R.id.progressBar);
        LogoImage  = findViewById(R.id.imageView);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity_login.this.onLoginClicked();
            }
        });

        textUsernameLayout.addTextChangedListener(createTextWatcher(textUsernameLayout));
        textPasswordInput.addTextChangedListener(createTextWatcher(textPasswordInput));
    }

    private void onLoginClicked() {
        String username = textUsernameLayout.getText().toString();
        String password = textPasswordInput.getText().toString();
        if (username.isEmpty()) {
            textUsernameLayout.setError("Username must not be empty");
        } else if (password.isEmpty()) {
            textPasswordInput.setError("Password must not be empty");
        }
        else {
            performLogin();
        }
    }

    //listener for changes in input layout
    private TextWatcher createTextWatcher(EditText textPasswordInput) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s,
                                          int start, int count, int after) {
                // not needed
            }

            @Override
            public void onTextChanged(CharSequence s,
                                      int start, int before, int count) {
                textPasswordInput.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {
                // not needed
            }
        };
    }

    private void performLogin() {
        textUsernameLayout.setEnabled(false);
        textPasswordInput.setEnabled(false);
        loginButton.setVisibility(View.INVISIBLE);
        //progressBar.setVisibility(View.VISIBLE);

        Handler handler = new Handler();
        //setTimeOut({}, 2000)
        handler.postDelayed(() -> {
            startMainActivity();
        }, 300);
    }

    private void startMainActivity() {
        // Search for MainActivity class
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}